FROM eclipse-temurin:17-jdk-alpine

VOLUME /tmp

COPY target/spring-petclinic-3.1.0-SNAPSHOT.jar /

ENTRYPOINT ["java","-jar","/spring-petclinic-3.1.0-SNAPSHOT.jar"]
